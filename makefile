#CC : le compilateur à utiliser
CC=gcc

#CFLAGS : les options de compilation  
CFLAGS= -Wall -Wextra -g

# les librairies à utiliser 
LIBS=

#LDFLAGS : les options d'édition de lien
LDFLAGS=

#lieu où se trouve les sources :
SRC=./src/

#les fichiers objets
OBJ=$(SRC)main.o \
	$(SRC)menu.o \
	$(SRC)error.o 



menu : $(OBJ)
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

$(SRC)%.o: $(SRC)%.c $(SRC)%.h
	$(CC) $(CFLAGS)  $(LDFLAGS) -DVERBOSE -c $< -o $@



clean:
	rm -rf $(SRC)*.o out

