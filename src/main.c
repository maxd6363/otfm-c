/**
 * @brief      Point d'entrée du programme
 *
 * @author     Maxime POULAIN - Lilian HERTEL
 * @date       2021
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "color.h"
#include "error.h"
#include "menu.h"

/**
 * @brief      Point d'entrée du programme
 *
 * @param[in]  argc  Le nombre d'argument
 * @param      argv  Le tableau contenant les arguments
 *
 * @return     Code d'erreur du programme
 */
int main(){
	const char items [3][MAX_STRING_SIZE] = {"Option #1","Option #2","Option #3"};
	const char *title = "This is a nice title !";
	char choice;
	menuDefault(items, sizeof(items)/MAX_STRING_SIZE,title);

	choice = menuManagement(items,sizeof(items)/MAX_STRING_SIZE,title);
	while(choice != 'Q'){
		switch(choice){
			case '1':
			printf("%s it is !", items[0]);
			break;
			case '2':
			printf("%s it is !", items[1]);
			break;
			case '3':
			printf("%s it is !", items[2]);
			break;
		}

		getchar();
		choice=menuManagement(items,sizeof(items)/MAX_STRING_SIZE,title);
	}

	return 0;
}
